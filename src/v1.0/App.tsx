import * as React from 'react';
import { Widget } from '@morphysm/react-chat-widget-1.0';

export interface AppProps {
  websocketURL: string;
  audioEnabled: boolean;
  userIcon?: string;
  closeIcon?: string;
  sendIcon?: string;
  widgetIcon?: string;
}

const App: React.FC<AppProps> = ({
  websocketURL,
  audioEnabled,
  userIcon,
  closeIcon,
  sendIcon,
  widgetIcon,
}: AppProps) => {
  return (
    <div className='App'>
      <Widget
        websocketURL={websocketURL}
        audioEnabled={audioEnabled}
        userIcon={userIcon}
        closeIcon={closeIcon}
        sendIcon={sendIcon}
        widgetIcon={widgetIcon}
      />
    </div>
  );
};

export default App;
