import * as React from 'react';
import * as ReactDOM from 'react-dom';

import App from './App';

interface WidgetOptions {
  rootId: string;
  websocketURL: string;
  userIcon?: string;
  closeIcon?: string;
  sendIcon?: string;
  widgetIcon?: string;
}

export const addWidget = ({
  rootId,
  websocketURL,
  userIcon,
  closeIcon,
  sendIcon,
  widgetIcon,
}: WidgetOptions) => {
  let mountNode = document.getElementById(rootId);
  ReactDOM.render(
    <App
      websocketURL={websocketURL}
      audioEnabled={false}
      userIcon={userIcon}
      closeIcon={closeIcon}
      sendIcon={sendIcon}
      widgetIcon={widgetIcon}
    />,
    mountNode
  );
};
