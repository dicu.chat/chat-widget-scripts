const webpack = require('webpack');
const path = require('path');

const config = {
  entry: {
    './': './src/v1.1/index.tsx',
    'v1.0': './src/v1.0/index.tsx',
    'v1.1': './src/v1.1/index.tsx'
  },
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: '[name]/bundle.js',
    library: 'chatWidget',
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        use: 'babel-loader',
        exclude: /node_modules/,
      },
      {
        test: /\.ts(x)?$/,
        loader: 'ts-loader',
        exclude: /node_modules/,
      },
      {
        test: /\.svg$/,
        use: 'file-loader',
      },
    ],
  },
  resolve: {
    extensions: ['.tsx', '.ts', '.js'],
  },
};

module.exports = config;
