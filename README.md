<h1 align="center">
  <br>
  <a href="http://www.amitmerchant.com/electron-markdownify"><img src="./img/morph_logo_rgb.png" alt="Markdownify" ></a>
  <br>
  <h5 align="center">Morphysm is a community of engineers, designers and researchers
contributing to security, cryptography, cryptocurrency and AI.</h5>
  <br>
</h1>

<h1 align="center">
 <img src="https://img.shields.io/npm/v/npm" alt="npm badge">
 <img src="https://img.shields.io/badge/React-17.0.2-orange" alt="react badge">
 <img src="https://img.shields.io/badge/version-1.0.0-lightgrey" alt="version badge">
 <img src="https://img.shields.io/badge/AI-Chatbot-green" alt="chatbot badge">
</h1>



# Dicu.chat chat-widget-scripts
This project provides scripts to integrate the Dicu.chat AI chatbot widget into any HTML/JS website.

# How It Works
The widget communicates with the web bridge of the core server using a web socket "[socket.io-client](https://www.npmjs.com/package/socket.io-client)", it internally stores all messages received through the web socket and displays them in the widget UI. To respond the user can select buttons or write text that is returned through the websocket.

# Website Integration

To add our widget to your ***website***, please fellow these steps:

- Add div with id of your choice to your html as a root for the chat widget.
- Import `www.dicu.chat/dist/bundle.js` script
- Run `window.chatWidget.addWidget(options)` with options object
  -  rootId: id of your root div
  -  websocketURL: url of the chat websocket
  -  userIcon: optional user icon, if not set standard user icon is used

```html
<div id="chat-widget-root">


<script type="text/javascript" src="https://dicu.chat/dist/bundle.js"></script>
<script>
  window.chatWidget.addWidget({
    rootId: "chat-widget-root",
    websocketURL: "wss://0f411020c4.a91ad78fad90f2d5aa4f51e0.com",
    userIcon: "./img/userIcon.png",
    closeIcon: "./img/closeIcon.png",
    sendIcon: "./img/sendIcon.png",
    widgetIcon: "./img/widgetIcon.png"
  });
</script>
```

# Local Development

## Installing
To run this project you have to following these steps:

1. git clone this repo

    ```
    git clone https://gitlab.com/dicu.chat/react-chat-widget.git
    ```
2. run npm install

    ```
    npm i @morphysm/react-chat-widget
    ```
## Develop 

In order to run our widget locally you can use this command: 
```
npm run start
```

# Demo

[![Demo Dicu widget](./img/widget.gif)](https://www.npmjs.com/package/@morphysm/react-chat-widget)

<br>

# Code Owners

@morphysm/team    :sunglasses:

# License
Our reposiorty is licensed under the terms of the GPLv3 see the license [here!](https://gitlab.com/dicu.chat/chat-widget-scripts/-/blob/master/LICENSE)

# Contact
If you'd like to know more about  us [Click here!](https://www.morphysm.com/), or You can contact us at "contact@morphysm.com".





